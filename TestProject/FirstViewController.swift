//
//  FirstViewController.swift
//  TestProject
//
//  Created by Sachin Kumar on 22/08/18.
//  Copyright © 2018 Sachin Kumar. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func clickMeAction(_ sender: Any) {
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "secondViewController") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
